package sabatu.org.spaceadventure.Items;

import java.util.Random;
import java.util.UUID;

/**
 * Created by Sabatu on 5/13/2017.
 */


public class InflateItem {

    /*Here all notion of type safety has been abandoned. Why? You might ask. Why couldn't the developer conceive of a
    more elegant solution to such a straight-forward problem? Well, let me tell you; friend. I did spend a lot of time considering this,
     and worked up a few other mock-ups of how I could make this work with very explicit type safety. But, let me say that I found myself
     disturbed with the idea that I was re-working these structures to neatly fall within the confines of very specific set of guidelines.
     What kind of structure is that to build? What kind of narrow perspective is that to conceive building solutions to problems through..
     that even though the original designers of the language specifically allowed it, it still shouldn't be used because it's "bad practice."
     I considered that it's only bad practice because we don't have enough faith in ourselves, or others, to accept the greater responsibilities
     such design decisions thrust upon us.Specifically, all this little T here does is put the full responsibility on the programmer to implement
     a receiving structure that understands this dynamic return of arbitrary type. Rest assured, I am the only programmer on both ends, and I understand it completely.

     */
    public static <T> T returnItem(String objName)
    {
        if(objName.equals("Blaster"))
        {

            Random numOfCharges = new Random();
            int minRounds = 3;
            int maxRounds = 30;


            EnergyWeapon Blaster = new EnergyWeapon();

            Blaster.setName("Blaster");

            Blaster.setType("Primary");

            //Sets the number of charges with the weapon to a random between range minRounds and maxRounds
            Blaster.setNumOfCharges(numOfCharges.nextInt(maxRounds - minRounds + 1) + minRounds);

            Blaster.setUniqueItemID(UUID.randomUUID().toString());

            Blaster.setAccuracy(8);

            Blaster.setRange(4);

            Blaster.setDamage(7);

            Blaster.setDescription("A provincial authorities close-quarter blaster. Puts out a fair bit of damage, and very accurate. " +
                    "The heat from these charges dissipates fast though. It was intentionally designed this way, as a safety mechanism" +
                    "against stray rounds causing unintended damage. Due to this design consideration, this blaster is meant for close-quarters" +
                    "altercations only, and consequently doesn't have much of am effective range.");

            return (T) Blaster;
        }

        if(objName.equals("Knife"))
        {
            KineticWeapon knife = new KineticWeapon();

            knife.setRange(1);

            knife.setType("secondaryWeapon");

            knife.setUniqueItemID(UUID.randomUUID().toString());

            knife.setName("Knife");

            knife.setDamage(10);

            knife.setAccuracy(10);

            knife.setDescription("An old knife you've kept on your belt for many years. It was given to you by a close friend before you left Earth." +
                    "The blade is a little chipped in a few places and the wooden handle is worn with sweat, but it's always been there when you needed it.");

            return (T) knife;
        }

        if(objName.equals("First Aid Kit"))
        {
            medicalKit FirstAidKit = new medicalKit();

            FirstAidKit.setName("First Aid Kit");

            FirstAidKit.setUniqueItemID(UUID.randomUUID().toString());

            FirstAidKit.setDescription("A standard medical kit, meant to treat light wounds.");

            FirstAidKit.setHealHPby(3);

            return (T) FirstAidKit;
        }

        if(objName.equals("Flashlight"))
        {

            Device Flashlight = new Device();

            Flashlight.setName("Flashlight");
            Flashlight.setUniqueItemID(UUID.randomUUID().toString());
            Flashlight.setDescription("An old flashlight you picked out of an emergency kit. Might come in handy.");


            return (T) Flashlight;

        }

        return null;
    }
}
