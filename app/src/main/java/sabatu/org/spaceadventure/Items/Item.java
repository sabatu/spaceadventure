package sabatu.org.spaceadventure.Items;

import java.util.UUID;

/**
 * Created by Sabatu on 5/13/2017.
 */

public abstract class Item {


    private String name = new String();
    private String description = new String();
    private String uniqueItemID = new String();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUniqueItemID() {
        return uniqueItemID;
    }

    public void setUniqueItemID(String uniqueItemID) {
        this.uniqueItemID = uniqueItemID;
    }

}