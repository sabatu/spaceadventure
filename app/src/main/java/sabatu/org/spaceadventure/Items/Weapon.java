package sabatu.org.spaceadventure.Items;

/**
 * Created by Sabatu on 5/15/2017.
 */

public class Weapon extends Item {

    private int numOfCharges = 0;
    private int accuracy = 0;
    private int range = 0;
    private int damage = 0;
    private String type = new String();

    public int getNumOfCharges() {
        return numOfCharges;
    }

    public void setNumOfCharges(int numOfCharges) {
        this.numOfCharges = numOfCharges;
    }

    public int getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(int accuracy) {
        this.accuracy = accuracy;
    }

    public int getRange() {
        return range;
    }

    public void setRange(int range) {
        this.range = range;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }


}
