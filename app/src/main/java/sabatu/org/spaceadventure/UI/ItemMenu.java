package sabatu.org.spaceadventure.UI;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import sabatu.org.spaceadventure.R;
import sabatu.org.spaceadventure.UI.InventoryDetailFragment;
import sabatu.org.spaceadventure.UI.InventorySelectFragment;

public class ItemMenu extends AppCompatActivity implements InventorySelectFragment.selectedComponent, InventoryDetailFragment.equippedComponent {

    boolean[] Equipped;
    String[] charPossessions;
    String[] currentlyEquipped;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_menu);

         Intent intent = getIntent();
         Equipped = intent.getBooleanArrayExtra("slots");
         charPossessions = intent.getStringArrayExtra("possessions");
         currentlyEquipped = intent.getStringArrayExtra("currentlySelected");

        //Activity fragments are defined here
        FragmentManager fm = getSupportFragmentManager();
        Fragment buttonMenuFragment = fm.findFragmentById(R.id.inventoryselectfragment_container);
        Fragment menuDetailFragment = fm.findFragmentById(R.id.inventorydetailfragment_container);

        //Pass through the initial list of variables sent from InteractiveScreen, to the selection fragment
        Bundle initialVariables = new Bundle();
        initialVariables.putBooleanArray("slots", Equipped);
        initialVariables.putStringArray("possessions",charPossessions);
        initialVariables.putStringArray("currentlySelected", currentlyEquipped);



        if (buttonMenuFragment == null) {
            buttonMenuFragment = new InventorySelectFragment();
            fm.beginTransaction()
                    .add(R.id.inventoryselectfragment_container, buttonMenuFragment)
                    .commit();


            if (menuDetailFragment == null) {
                menuDetailFragment = new InventoryDetailFragment();
                fm.beginTransaction()
                        .add(R.id.inventorydetailfragment_container, menuDetailFragment)
                        .commit();
            }

            //Send initial values to both fragments
            buttonMenuFragment.setArguments(initialVariables);
            menuDetailFragment.setArguments(initialVariables);


        }


    } //<== end of onCreate()


    public void onItemSelected(String itemName){

        InventoryDetailFragment invDetailFrag = (InventoryDetailFragment)
                getSupportFragmentManager().findFragmentById(R.id.inventorydetailfragment_container);

        if(invDetailFrag != null)
        {
            //Pass selected item to displayItemDetail Fragment


            invDetailFrag.receiveItemForDisplay(itemName);
            //InventoryDetailFragment.getInstance().receiveItemForDisplay(itemName);

        }

        //Function for receiving button click from itemSelectFragment


    }

    public void onExitButtonSelected()
    {
        Intent sendData = new Intent();
        sendData.putExtra("slots", Equipped);
        sendData.putExtra("currentlyEquipped", currentlyEquipped);
        sendData.putExtra("possessions", charPossessions);
        setResult(RESULT_OK, sendData);

        //Now, destroy everything.

        finish();


    }




   //This function gets called by InventoryDetailFragment, when a user equips an item for use.
    public void onItemEquipped(String itemName)
    {
        if(currentlyEquipped == null)
        {
            currentlyEquipped = new String[1];
            currentlyEquipped[0] = itemName;

        }
        else {
            List<String> addToEquipped = new ArrayList<>(Arrays.asList(currentlyEquipped));
            addToEquipped.add(itemName);

            //Exterminates filthy nulls that try to sneak into the array. Not today, bitches!
            while (addToEquipped.remove(null)) ;
            currentlyEquipped = addToEquipped.toArray(new String[addToEquipped.size()]);
        }

    }

    public void onItemUnEquipped(String itemName)
    {
        List<String> removeFromEquipped = new ArrayList<>(Arrays.asList(currentlyEquipped));
        removeFromEquipped.remove(itemName);

        //If the last element was removed, then cleanse the array
        if(removeFromEquipped.isEmpty())
        {
            currentlyEquipped = new String[0];
        }
        else
        {
            currentlyEquipped = removeFromEquipped.toArray(new String[removeFromEquipped.size()]);
        }


    }



} // <== end of class ItemMenu()