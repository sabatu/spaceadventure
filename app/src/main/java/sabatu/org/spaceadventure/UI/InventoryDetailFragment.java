package sabatu.org.spaceadventure.UI;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import sabatu.org.spaceadventure.Items.EnergyWeapon;
import sabatu.org.spaceadventure.Items.InflateItem;
import sabatu.org.spaceadventure.Items.Item;
import sabatu.org.spaceadventure.Items.KineticWeapon;
import sabatu.org.spaceadventure.Items.medicalKit;
import sabatu.org.spaceadventure.R;

import static android.graphics.Color.BLACK;

/**
 * Created by Sabatu on 5/15/2017.
 */

public class InventoryDetailFragment extends Fragment {


 static InventoryDetailFragment instance;


    equippedComponent mCallback;


    public interface equippedComponent {

        public void onItemEquipped(String itemName);
        public void onItemUnEquipped(String itemName);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {

            mCallback = (equippedComponent) context;

        }catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + "must implement onItemSelected");
        }

    } //<=== end of onAttach()

    private String[] currentPossessions;
    private String[] currentlySelected;
    private boolean[] Equipped;

    //Add Equip / Use buttons

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        instance = this;

        //Set initial game variables on fragment creation

        Equipped = getArguments().getBooleanArray("slots");
        currentPossessions = getArguments().getStringArray("possessions");
        currentlySelected =  getArguments().getStringArray("currentlySelected");



    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.inventorydetaillayout, container, false);



        return v;
    }

    //Creates a static instance of this fragment that can be accessed from the fragments calling activity.
    public static InventoryDetailFragment getInstance()
    {

        if(instance == null){
            instance = new InventoryDetailFragment();
        }

        return instance;

    }



    public void receiveItemForDisplay(String itemName)
    {
        //Display Item on screen

        int id = 0;

        LinearLayout ll = (LinearLayout) getView().findViewById(R.id.InventoryDetailFragmentLayout);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);

        ll.removeAllViews();

        Button useButton = new Button((getActivity().getApplicationContext()));
        useButton.setText("Use");
        useButton.setId(id++);
        useButton.setEnabled(false);

        final Button EquipButton = new Button((getActivity().getApplicationContext()));


       if(currentlySelected == null) {
           EquipButton.setText("Equip");
       }
       else
       {
           List<String> checkIfEquipped = new ArrayList<>(Arrays.asList(currentlySelected));

           if(checkIfEquipped.contains(itemName))
           {
               EquipButton.setText("Unequip");
           }
           else
           {
               EquipButton.setText("Equip");
           }
       }


        EquipButton.setId(id++);
        EquipButton.setEnabled(false);

        //Instantiate Item
        final Item selectedItem = InflateItem.returnItem(itemName);

        //Set title text as name

        TextView nameTitle = new TextView((getActivity().getApplicationContext()));
        nameTitle.setText(selectedItem.getName());
        nameTitle.setId(id++);
        nameTitle.setTextColor(BLACK);
        ll.addView(nameTitle, lp);

        /*Add individual detail fields, depending on type instance. If you want to keep the
        description generic to title and description, dont bother with these conditionals.
        */
        if(selectedItem instanceof EnergyWeapon)
        {
            TextView numOfCharges = new TextView((getActivity().getApplicationContext()));
            numOfCharges.setText("Number of charges: " + ((EnergyWeapon) selectedItem).getNumOfCharges());
            numOfCharges.setId(id++);
            numOfCharges.setTextColor(BLACK);
            ll.addView(numOfCharges, lp);

            TextView damage = new TextView((getActivity().getApplicationContext()));
            damage.setText("Damage: " + ((EnergyWeapon) selectedItem).getDamage());
            damage.setId(id++);
            damage.setTextColor(BLACK);
            ll.addView(damage, lp);

            TextView accuracy = new TextView((getActivity().getApplicationContext()));
            accuracy.setText("Accuracy: " + ((EnergyWeapon) selectedItem).getAccuracy());
            accuracy.setId(id++);
            accuracy.setTextColor(BLACK);
            ll.addView(accuracy, lp);

            TextView range = new TextView((getActivity().getApplicationContext()));
            range.setText("Range: " + ((EnergyWeapon) selectedItem).getRange());
            range.setId(id++);
            range.setTextColor(BLACK);
            ll.addView(range, lp);

            EquipButton.setEnabled(true);
        }

        if(selectedItem instanceof medicalKit)
        {
            TextView healHpby = new TextView((getActivity().getApplicationContext()));
            healHpby.setText("This first aid kit will recover: " + ((medicalKit) selectedItem).getHealHPby() + " points of damage.");
            healHpby.setId(id++);
            healHpby.setTextColor(BLACK);
            ll.addView(healHpby, lp);

            useButton.setEnabled(true);

        }

        if(selectedItem instanceof KineticWeapon)
        {

            TextView damage = new TextView((getActivity().getApplicationContext()));
            damage.setText("Damage: " + ((KineticWeapon) selectedItem).getDamage());
            damage.setId(id++);
            damage.setTextColor(BLACK);
            ll.addView(damage, lp);

            TextView accuracy = new TextView((getActivity().getApplicationContext()));
            accuracy.setText("Accuracy: " + ((KineticWeapon) selectedItem).getAccuracy());
            accuracy.setId(id++);
            accuracy.setTextColor(BLACK);
            ll.addView(accuracy, lp);

            TextView range = new TextView((getActivity().getApplicationContext()));
            range.setText("Range: " + ((KineticWeapon) selectedItem).getRange());
            range.setId(id++);
            range.setTextColor(BLACK);
            ll.addView(range, lp);

            EquipButton.setEnabled(true);


        }

        TextView descriptionFooter = new TextView((getActivity().getApplicationContext()));
        descriptionFooter.setText(selectedItem.getDescription());
        descriptionFooter.setId(id++);
        descriptionFooter.setTextColor(BLACK);
        ll.addView(descriptionFooter, lp);


        ll.addView(useButton, lp);
        ll.addView(EquipButton, lp);

        //Add implementation for use button, once character classes have been implemented.

        EquipButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                if(currentlySelected == null)
                {
                    currentlySelected = new String[1];
                    currentlySelected[0] = selectedItem.getName();
                    mCallback.onItemEquipped(selectedItem.getName()); //Still null at the receiving end..
                    EquipButton.setText("Unequip");
                }
                else {
                    List<String> currentSel = new ArrayList<>(Arrays.asList(currentlySelected));


                    //Check to see if item is already Equipped. If it is, then unequip it.
                    if (currentSel.contains(selectedItem.getName())) {
                        mCallback.onItemUnEquipped(selectedItem.getName());

                        //Remove local copy, too.
                        currentSel.remove(selectedItem.getName());
                        currentlySelected = currentSel.toArray(new String[currentSel.size()]);

                        EquipButton.setText("Equip");
                    } else {
                        //Otherwise, equip it.
                        mCallback.onItemEquipped(selectedItem.getName());

                        EquipButton.setText("Unequip");

                        //Add it to the list..
                        currentSel.add(selectedItem.getName());
                        while (currentSel.remove(null)) ;
                        currentlySelected = currentSel.toArray(new String[currentSel.size()]);
                    }
                }


            }

        });




    }


}


