package sabatu.org.spaceadventure.UI;


import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import sabatu.org.spaceadventure.R;

public class InteractiveScreen extends AppCompatActivity implements MainPanelFragment.activityCommunication {

    String[] characterPossessions;
    String[] currentlySelected;
    boolean[] Equipped;


    @Override
    protected void onCreate(Bundle savedInstanceState) {


        //These are the initial variables/state that the game begins in
        characterPossessions = new String[]{"Blaster","Knife", "First Aid Kit", "Flashlight"};
        currentlySelected = new String[0];
        Equipped = new boolean[]{false, false, false};

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interactive_screen);

        FragmentManager fm = getSupportFragmentManager();
        Fragment mainPanelFragment = fm.findFragmentById(R.id.fragment_container);

        Bundle initialData = new Bundle();

        //Send initial character data to main panel fragment
        initialData.putBooleanArray("slots", Equipped);
        initialData.putStringArray("possessions", characterPossessions);
        initialData.putStringArray("currentlySelected", currentlySelected);


        if (mainPanelFragment == null) {
            mainPanelFragment = new MainPanelFragment();
            fm.beginTransaction()
                    .add(R.id.fragment_container,mainPanelFragment)
                    .commit();
        }

        //Remember, the arguments dont get set until after the add transaction has been committed
        mainPanelFragment.setArguments(initialData);




    } //<== end of onCreate()

    public void itemMenuSelected(boolean[] slots, String[] possessions, String[] currentlyEquipped)
    {
        Intent intent = new Intent(this, ItemMenu.class);
        intent.putExtra("slots", slots);
        intent.putExtra("possessions", possessions);
        intent.putExtra("currentlySelected", currentlyEquipped);
        startActivityForResult(intent, 1);


    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        Equipped = data.getBooleanArrayExtra("slots");
        characterPossessions = data.getStringArrayExtra("possessions");
        currentlySelected = data.getStringArrayExtra("currentlyEquipped");

        MainPanelFragment updatefrag = (MainPanelFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_container);
        updatefrag.updateInventoryOnScreen(Equipped, currentlySelected, updatefrag.getView());




    }



}
