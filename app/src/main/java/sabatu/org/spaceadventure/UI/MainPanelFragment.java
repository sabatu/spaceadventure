package sabatu.org.spaceadventure.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import sabatu.org.spaceadventure.Items.Equipment;
import sabatu.org.spaceadventure.Items.InflateItem;
import sabatu.org.spaceadventure.Items.Item;
import sabatu.org.spaceadventure.Items.Weapon;
import sabatu.org.spaceadventure.R;
import sabatu.org.spaceadventure.UI.InventorySelectFragment;

import static sabatu.org.spaceadventure.R.id.current_primary_weapon;
import static sabatu.org.spaceadventure.R.id.current_secondary_weapon;
import static sabatu.org.spaceadventure.R.id.current_utility_item;
import static sabatu.org.spaceadventure.R.id.textView;

/**
 * Created by Sabatu on 5/14/2017.
 */

public class MainPanelFragment extends Fragment {

    activityCommunication mCallback;

    public interface activityCommunication{

        public void itemMenuSelected(boolean[] slots, String[] possessions, String[] currentlyEquipped);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {

            mCallback = (activityCommunication) context;

        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + "must implement onItemSelected");
        }
    }




    private String[] characterPossessions = new String[0];
    private String[] currentlySelected = new String[0];
    private boolean[] Equipped = new boolean[3];




    private static final int requestCode = 0;

    private Button mMoveForwardButton;
    private Button mMoveBackwardsButton;
    private Button mMoveLeftButton;
    private Button mMoveRightButton;
    private Button mAccessItemMenu;

    private TextView mainPanel;
    private TextView currentPriWeapon;
    private TextView currentSecondWeapon;
    private TextView currentUtilityItem;



    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        //Initialize all variables from activity.
        Equipped = getArguments().getBooleanArray("slots");
        currentlySelected = getArguments().getStringArray("currentlySelected");
        characterPossessions = getArguments().getStringArray("possessions");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mainpanelfragment, container, false);


        mainPanel = (TextView) v.findViewById(textView);

        final Context myContext = this.getActivity().getApplicationContext();


        currentPriWeapon = (TextView) v.findViewById(current_primary_weapon);
        currentSecondWeapon = (TextView) v.findViewById(current_secondary_weapon);
        currentUtilityItem = (TextView) v.findViewById(current_utility_item);

        updateInventoryOnScreen(Equipped, currentlySelected, v);




        mMoveForwardButton = (Button) v.findViewById(R.id.move_forward_button);
        mMoveForwardButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mainPanel.setText("You run into the docking bay and seal the door behind you. Something smashes into the door. The door keeps its integrity, for now.");


            }


        });


        mMoveBackwardsButton = (Button) v.findViewById(R.id.move_backwards_button);
        mMoveBackwardsButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mainPanel.setText("Something strikes you hard in the back. Your vision clouds, and everything goes dark as you fall to the floor unconscious.");


            }

        });


        mMoveLeftButton = (Button) v.findViewById(R.id.move_left_button);
        mMoveLeftButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mainPanel.setText("You slam your body into the left side of the corridor. The scuffling noise behind you is getting closer, and sounds rather violent.");


            }

        });


        mMoveRightButton = (Button) v.findViewById(R.id.move_right_button);
        mMoveRightButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mainPanel.setText("You slam your body into the right side of the corridor. The scuffling noise behind you is getting closer, and sounds rather violent.");


            }

        });


        mAccessItemMenu = (Button) v.findViewById(R.id.access_item_menu);
        mAccessItemMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mCallback.itemMenuSelected(Equipped, characterPossessions, currentlySelected);

            }

        });



        return v;
    }

    public void updateInventoryOnScreen(boolean[] Equipped, String[] currentSelected, View view)
    {
        currentPriWeapon = (TextView) view.findViewById(current_primary_weapon);
        currentSecondWeapon = (TextView) view.findViewById(current_secondary_weapon);
        currentUtilityItem = (TextView) view.findViewById(current_utility_item);

        currentlySelected = currentSelected;


        boolean exitCondition = true;

        if(currentSelected == null)
        {
            currentPriWeapon.setText("");
            currentSecondWeapon.setText("");
            currentUtilityItem.setText("");
        }
        else {

            List<String> cleanedSelected = new ArrayList<>(Arrays.asList(currentSelected));

            while (cleanedSelected.remove(null)) ;

            currentSelected = cleanedSelected.toArray(new String[cleanedSelected.size()]);

            List<Item> comparisonItems = new ArrayList<>();

            int index = 0;

            while (currentSelected.length != index) {

                comparisonItems.add((Item) InflateItem.returnItem(currentSelected[index]));
                index += 1;
            }

            //Start of item selection

            if (!cleanedSelected.contains("Blaster")) {
                currentPriWeapon.setText("");
            } else {
                currentPriWeapon.setText("Blaster");
            }

            if (!cleanedSelected.contains("Knife")) {
                currentSecondWeapon.setText("");
            } else {
                currentSecondWeapon.setText("Knife");
            }

            if (!Equipped[2]) {
                currentUtilityItem.setText("");
            } else {
                Iterator<Item> currentItemIT = comparisonItems.iterator();

                while (currentItemIT.hasNext() && exitCondition) {
                    Item compare = currentItemIT.next();
                    if (compare instanceof Equipment) {
                        currentUtilityItem.setText(compare.getName());
                        exitCondition = false;
                    }
                }
                exitCondition = true;

            }

        }





    } // <===Update inventory on screen


    public void sendInitialVariables(boolean[] slots, String[] possessions, String[] currentlyEquipped)
    {

    }

} //< == MainPanelFragment
