package sabatu.org.spaceadventure.UI;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import sabatu.org.spaceadventure.Items.InflateItem;
import sabatu.org.spaceadventure.Items.Item;
import sabatu.org.spaceadventure.R;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Sabatu on 5/14/2017.
 */

public class InventorySelectFragment extends Fragment {


    selectedComponent mCallback;


    public interface selectedComponent {

        public void onItemSelected(String itemName);
        public void onExitButtonSelected();

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {

            mCallback = (selectedComponent) context;

        }catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + "must implement onItemSelected");
        }

    } //<=== end of onAttach()



    private TextView itemMenuText;

    int charPossCount = 0;

    private String[] currentPossessions;
    private String[] currentlySelected;
    private boolean[] Equipped;


        @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);


            //Set initial game variables on fragment creation

            Equipped = getArguments().getBooleanArray("slots");
            currentPossessions = getArguments().getStringArray("possessions");
            currentlySelected =  getArguments().getStringArray("currentlySelected");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.inventoryselectfragment, container, false);

        itemMenuText = (TextView) v.findViewById(R.id.itemMenuTextWindow);


        final List<String> currentlySelectedList = new ArrayList<>();

        if(currentlySelected != null) {

            List<String>passedInList = new ArrayList<>(Arrays.asList(currentlySelected));
            Iterator<String> passedInListIT = passedInList.iterator();

            while(passedInListIT.hasNext())
            {
                currentlySelectedList.add(passedInListIT.next());
            }

        }
        //Ensure any nulls passed into the list get cleansed
        while(currentlySelectedList.remove(null));



        charPossCount = currentPossessions.length;

        int i = 0;

        List<Item> comparisonItems = new ArrayList<>();

        int index = 0;

        if(currentPossessions.length != 0 && (index != currentPossessions.length)) {


            while (index != (currentPossessions.length)) {
                comparisonItems.add((Item)InflateItem.returnItem(currentPossessions[index]));
                index += 1;
            }

        }
        else
        {

        }

        Iterator<Item> compItemsIT = comparisonItems.iterator();

        while(compItemsIT.hasNext())
        {

            final Item currentItem = compItemsIT.next();

            Button myButton = new Button((getActivity().getApplicationContext()));
            myButton.setText(currentItem.getName());
            myButton.setId(i);
            i += 1;



            LinearLayout ll = (LinearLayout)v.findViewById(R.id.InventorySelectFragmentLayout);
            ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
            ll.addView(myButton, lp);

            myButton.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    /*This calls the onItemSelected method defined in the parent activity, which sends the result of the button press
                    to InventoryDetailFragment, which will then process the click and post a detailed list of attributes for the selected
                    item to its view.
                      */

                    mCallback.onItemSelected(currentItem.getName());



                } // <=== End of onClick Listener
            }); // <=== End of onClick Instantiation



        } //<==End of dynamically adding buttons

        Button ExitItemMenu = new Button(getActivity().getApplicationContext());
        ExitItemMenu.setText("Exit Item Menu");
        ExitItemMenu.setId(i);


        LinearLayout ll = (LinearLayout) v.findViewById(R.id.InventorySelectFragmentLayout);
        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        ll.addView(ExitItemMenu, lp);

        ExitItemMenu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                //The activity will deal with returning to the main panel, via this function.
               mCallback.onExitButtonSelected();

            }


        });

        return v;

    }

    private void sendCurrentInventory(boolean[] Equipped, String[] currentlySelected)
    {

        Intent data = new Intent();
        data.putExtra("Equipped", Equipped);
        data.putExtra("Currently Selected", currentlySelected);
        getActivity().setResult(RESULT_OK,data);
        getActivity().finish();


    }

    public static boolean[] returnEquipped(Intent result)
    {
        return result.getBooleanArrayExtra("Equipped");
    }

    public static String[] returnSelected(Intent result)
    {
        return result.getStringArrayExtra("Currently Selected");
    }



}





      /*

                   // if(currentItem.getType().equals("PrimaryWeapon"))
                    {
                        if(Equipped[0] == true) //<< == user has selected an equipped item, so unequip it.
                        {
                            itemMenuText.setText("You have unequipped your " + currentItem.getName());
                            Equipped[0] = false;

                            currentlySelectedList.remove(currentItem.getName());

                        }
                        else //<< === User has selected to equip an item, so equip it.
                        {
                            itemMenuText.setText("You have equipped your " + currentItem.getName());
                            Equipped[0] = true;

                            currentlySelectedList.add(currentItem.getName());
                        }
                    } // <<==== End of Primary Weapon Listener

               //     if(currentItem.getType().equals("SecondaryWeapon"))
                    {
                        if(Equipped[1] == true)
                        {
                            itemMenuText.setText("You have unequipped your " + currentItem.getName());
                            Equipped[1] = false;

                            currentlySelectedList.remove(currentItem.getName());

                        }
                        else
                        {
                            itemMenuText.setText("You have equipped your " + currentItem.getName());
                            Equipped[1] = true;

                            currentlySelectedList.add(currentItem.getName());

                        }
                    } // <<==== End of Secondary Weapon Listener

               //     if(currentItem.getType().equals("Utility"))
                    {
                        if(Equipped[2] == true)
                        {
                            itemMenuText.setText("You have unequipped your " + currentItem.getName());
                            Equipped[2] = false;

                            currentlySelectedList.remove(currentItem.getName());

                        }
                        else
                        {
                            itemMenuText.setText("You have equipped your " + currentItem.getName());
                            Equipped[2] = true;

                            currentlySelectedList.add(currentItem.getName());

                        }


                        */